<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class RandomColorGeneratorController extends AbstractController
{
    /**
     * @Route("/randomColor", name="random_color_generator")
     */
    public function index()
    {
        return $this->render('random_color_generator/index.html.twig', [
            'controller_name' => 'RandomColorGeneratorController',
        ]);
    }
}
