/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)
require("../scss/app.scss");

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
const $ = require("jquery");

require("bootstrap");

import Vue from "vue";
import axios from "axios";
var app = new Vue({
  el: "#random",
  delimiters: ["{*", "*}"],
  data: {
    color: "#000000"
  },
  methods: {
    randomColor: function(event) {
      this.color = "#" + ((Math.random() * 0xffffff) << 0).toString(16);
      this.color2 = "#" + ((Math.random() * 0xffffff) << 0).toString(16);
      this.color3 = "#" + ((Math.random() * 0xffffff) << 0).toString(16);
      this.color4 = "#" + ((Math.random() * 0xffffff) << 0).toString(16);
      this.color5 = "#" + ((Math.random() * 0xffffff) << 0).toString(16);
      this.color6 = "#" + ((Math.random() * 0xffffff) << 0).toString(16);
      this.color7 = "#" + ((Math.random() * 0xffffff) << 0).toString(16);
      this.color8 = "#" + ((Math.random() * 0xffffff) << 0).toString(16);
      this.color9 = "#" + ((Math.random() * 0xffffff) << 0).toString(16);
      this.color10 = "#" + ((Math.random() * 0xffffff) << 0).toString(16);
      this.color11 = "#" + ((Math.random() * 0xffffff) << 0).toString(16);
      this.color12 = "#" + ((Math.random() * 0xffffff) << 0).toString(16);
      this.color13 = "#" + ((Math.random() * 0xffffff) << 0).toString(16);
      this.color14 = "#" + ((Math.random() * 0xffffff) << 0).toString(16);

      document.getElementById("randomColor1").style.background = this.color;
      document.getElementById("randomColor2").style.background = this.color2;
      document.getElementById("randomColor3").style.background = this.color3;
      document.getElementById("randomColor4").style.background = this.color4;
      document.getElementById("randomColor5").style.background = this.color5;
      document.getElementById("randomColor6").style.background = this.color6;
      document.getElementById("randomColor7").style.background = this.color7;
      document.getElementById("randomColor8").style.background = this.color8;
      document.getElementById("randomColor9").style.background = this.color9;
      document.getElementById("randomColor10").style.background = this.color10;
      document.getElementById("randomColor11").style.background = this.color11;
      document.getElementById("randomColor12").style.background = this.color12;
      document.getElementById("randomColor13").style.background = this.color13;
      document.getElementById("randomColor14").style.background = this.color14;
    }
  }
});

console.log("Hello Webpack Encore! Edit me in assets/js/app.js");
